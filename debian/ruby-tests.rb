# This strange way to run the tests (in independent processes) is inspired from timecop's Rakefile

  failed = Dir["test/*_test.rb"].map do |test|
    command = "ruby #{test}"
    puts
    puts command
    command unless system(command)
  end.compact
  if failed.any?
    abort "#{failed.count} Tests failed\n#{failed.join("\n")}"
  end

